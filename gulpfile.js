"use strict";
var requireOptions = {
   only: ['gulp', 'browser-sync']
};
var $ = require('auto-require')(requireOptions);
var del = require('del');
var notify = require("gulp-notify");
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var htmlmin = require('gulp-htmlmin');
var htmlhint = require("gulp-htmlhint");
var processhtml = require('gulp-processhtml');
var concatCss = require('gulp-concat-css');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');

var paths = {
   app: {
      path: './src',
      js: './src/**/*.js',
      css: './src/**/*.css',
      html: './src/**/*.html',
   },
   build: {
      path: './build',
   }
};

// minifier tasks
$.gulp.task('process-html', function () {
   return $.gulp.src(paths.app.html)
      .pipe(processhtml())
      .pipe(htmlmin({
         collapseWhitespace: true
      }))
      .pipe($.gulp.dest(paths.build.path));
});

$.gulp.task('concat-app-css', function () {
   return $.gulp.src(paths.app.css)
      .pipe(concatCss('style.css'))
      .pipe($.gulp.dest(paths.build.path + '/css'))
      .pipe($.browserSync.stream());
});

$.gulp.task('concat-app-js', function () {
   return $.gulp.src(paths.app.js)
      .pipe(sourcemaps.init())
      .pipe(concat('app.js'))
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe($.gulp.dest(paths.build.path + '/js'));
});


// code hints
$.gulp.task('html-hint', function () {
   return $.gulp.src(paths.app.html)
      .pipe(htmlhint('.htmlhintrc'))
      .pipe(htmlhint.reporter('htmlhint-stylish'))
      .pipe(notify(function (file) {
         if (file.htmlhint.success) {
            return false;
         }
         var errors = file.htmlhint.messages.map(function (data) {
            if (data.error) {
               return "(" + data.error.line + ':' + data.error.col + ') ' + data.error.message;
            }
         }).join("\n");
         return file.relative + " (" + file.htmlhint.errorCount + " errors)\n" + errors;
      }))
      .pipe(htmlhint.reporter('fail'));
});

$.gulp.task('js-hint', function () {
   return $.gulp.src(paths.app.js)
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
      .pipe(notify(function (file) {
         if (file.jshint.success) {
            return false;
         }
         var errors = file.jshint.results.map(function (data) {
            if (data.error) {
               return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
            }
         }).join("\n");
         return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
      }))
      .pipe(jshint.reporter('fail'));
});


// build taks 
$.gulp.task('clean', function () {
   return del([paths.build.path + '/**', '!' + paths.build.path]);
});

$.gulp.task('build', [
   'clean',
   'html-hint',
   'js-hint',
   'concat-app-js',
   'concat-app-css',
   'process-html',
], function () {
   $.gulp.src(paths.app.path + '/**/*.png')
      .pipe(imagemin())
      .pipe($.gulp.dest(paths.build.path + '/images'));
});

// others tasks
$.gulp.task('browser-sync-reload', function (done) {
   $.browserSync.reload();
   done();
});

$.gulp.task('serve', [
   'build',
], function () {

   $.gulp.watch(paths.app.js, ['concat-app-js']);
   $.gulp.watch(paths.app.css, ['concat-app-css']);
   $.gulp.watch(paths.app.html, ['process-html']);

   $.gulp.watch(paths.build.path + '/js/*.js', ['browser-sync-reload']);
   $.gulp.watch(paths.build.path + '/css/*.js', ['browser-sync-reload']);
   $.gulp.watch(paths.build.path + '/*.html', ['browser-sync-reload']);

   return $.browserSync.init({
      server: {
         baseDir: paths.build.path
      },
      ghostMode: {
         clicks: false,
         forms: false,
         scroll: false
      }
   });
});

$.gulp.task('default', ['serve']);